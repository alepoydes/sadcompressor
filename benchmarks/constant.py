import numpy as np
import os

import sad

SHP = (64,64,64)
NBITS = 7
MAXT = 100
DT = 0.1

def ones(_t):
    return np.ones(SHP, dtype=np.float32)

rand = np.random.rand(*SHP).astype(np.float32)
def constant(_t):
    return rand

def random(_t):
    return np.random.rand(*SHP).astype(np.float32)

x, y, z = [np.linspace(-1,1,s,dtype=np.float32) for s in SHP]
xx, yy, zz = np.meshgrid(x,y,z, indexing='ij')
def adiabatic(t, spd=0.01):
    return np.cos(xx+spd*t)*np.cos(yy+spd*t)*np.cos(zz+spd*t)

def compress(file, gen, maxt, dt=0.1):
    uncompressed_size = 0
    with sad.SADWriter(file, prec_maxexp=0, prec_nbits=NBITS) as c:
        while c.t<maxt:
            print(f"{c.t:.3f}                         ", end='\r')
            array = gen(c.t)
            uncompressed_size += array.nbytes
            c['x'] = array
            c.next_key(dt=dt)
    compressed_size = os.path.getsize(file)
    print(f"Uncompressed {uncompressed_size}")
    print(f"Compressed   {compressed_size}")
    print(f"Compression rate {uncompressed_size/compressed_size}")


if __name__ == "__main__":
    # compress(file="tmp/ones.sad", gen=ones, maxt=MAXT, dt=DT)    
    # compress(file="tmp/constant.sad", gen=constant, maxt=MAXT, dt=DT)  
    # compress(file="tmp/random.sad", gen=random, maxt=MAXT, dt=DT)  
    compress(file="tmp/adiabatic.sad", gen=adiabatic, maxt=MAXT, dt=DT) 
