from setuptools import setup
import unittest
from itertools import chain

# Kludge
# from sad._version import __version__ 
__version__ = "0.0.2"

extras_require = {
}

extras_require['all'] = list(set(chain(*map(extras_require.__getitem__, []))))

setup(name='sadcompressor',
      version=__version__,
      description='Streamed Array Data compressor',
      url='https://gitlab.com/alepoydes/sadcompressor.git',
      author='Igor Lobanov',
      author_email='lobanov.igor@gmail.com',
      license='GNU GPLv3',
      packages=['sad'],
      install_requires=['pytest','numpy','numba','py-ubjson','packaging','rich','termcolor'],
      # setup_requires=['pytest-runner'],
      # tests_require=['pytest'],
      extras_require=extras_require,
      scripts=['bin/sadump', 'bin/sadcopy', 'bin/sadinfo'],
      include_package_data=True,
      test_suite='tests',
      zip_safe=False
      )